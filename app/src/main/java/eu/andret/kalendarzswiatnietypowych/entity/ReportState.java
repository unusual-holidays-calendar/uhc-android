package eu.andret.kalendarzswiatnietypowych.entity;

public enum ReportState {
	REPORTED,
	APPLIED,
	DECLINED,
	ON_HOLD
}
